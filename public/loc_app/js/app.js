var status_user = 0;
var status_notif = 0;

function sub_menu_user(){
  if(status_user == 0){
    $('.sub_menu.user').css('display','block');
    $('.name_agency i').css('transform','rotate(180deg)');
    status_user = 1;
  }
  else{
    $('.sub_menu.user').css('display','none');
    $('.name_agency i').css('transform','rotate(0deg)');
    status_user = 0;
  }
}

function sub_menu_notif(){
  if(status_notif == 0){
    $('.sub_menu.notif').css('display','block');
    $('.notif_box span.notif_icon').css('background','#ebebeb');
    status_notif = 1;
  }
  else{
    $('.sub_menu.notif').css('display','none');
    $('.notif_box span.notif_icon').css('background','#F7F7F7');
    status_notif = 0;
  }
}

function actions_module_recap(event,id){
  
  if(event == 'edit'){
    alert('lien de redirection pour modifier la réservation');
  }
  
  else{
    
    if(event == 'view'){
      alert('lien de redirection pour voir la réservation');
    }
    
    else{
      
      if(event == 'delete'){
        modal_yes_no('Si vous souhaitez supprimer une réservation, tapez SUPPRIMER','Un email ainsi qu\'un sms sera envoyé pour avertir de la suppression','123');
      }
      
      else{
        
        if(event == 'validate'){
          alert('validation de la réservation');
        }
        
        else{
          alert('Notification : Une erreutr s\'est produite');
        }
        
      }
      
    }
    
  }
  
}

function modal_yes_no(question,informations,element_sup){
  alert(informations);
}