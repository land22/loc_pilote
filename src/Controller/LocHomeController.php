<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class LocHomeController extends AbstractController
{
    /**
     * @Route("/", name="loc_home")
     */
    public function index()
    {
        return $this->render('loc_home/index.html.twig');
    }
    /**
     * @Route("/dashbord", name="dashbord")
     */
    public function dashbord()
    {
        return $this->render('loc_home/dashbord.html.twig', [
            'controller_name' => 'LocHomeController',
        ]);
    }


}
