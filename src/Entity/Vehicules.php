<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VehiculesRepository")
 */
class Vehicules
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $matriculation;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url_photo;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $couleur;

    /**
     * @ORM\Column(type="integer")
     */
    private $prixLocation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $note;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $active;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $modele;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateVidange;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateVisiteTechnique;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateFinDassurance;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Marque", inversedBy="vehicule")
     */
    private $marque;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Users", inversedBy="vehicules")
     */
    private $user;

    public function __construct()
    {
        $this->user = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMatriculation(): ?string
    {
        return $this->matriculation;
    }

    public function setMatriculation(string $matriculation): self
    {
        $this->matriculation = $matriculation;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getUrlPhoto(): ?string
    {
        return $this->url_photo;
    }

    public function setUrlPhoto(string $url_photo): self
    {
        $this->url_photo = $url_photo;

        return $this;
    }

    public function getCouleur(): ?string
    {
        return $this->couleur;
    }

    public function setCouleur(?string $couleur): self
    {
        $this->couleur = $couleur;

        return $this;
    }

    public function getPrixLocation(): ?int
    {
        return $this->prixLocation;
    }

    public function setPrixLocation(int $prixLocation): self
    {
        $this->prixLocation = $prixLocation;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getModele(): ?string
    {
        return $this->modele;
    }

    public function setModele(?string $modele): self
    {
        $this->modele = $modele;

        return $this;
    }

    public function getDateVidange(): ?\DateTimeInterface
    {
        return $this->dateVidange;
    }

    public function setDateVidange(?\DateTimeInterface $dateVidange): self
    {
        $this->dateVidange = $dateVidange;

        return $this;
    }

    public function getDateVisiteTechnique(): ?\DateTimeInterface
    {
        return $this->dateVisiteTechnique;
    }

    public function setDateVisiteTechnique(?\DateTimeInterface $dateVisiteTechnique): self
    {
        $this->dateVisiteTechnique = $dateVisiteTechnique;

        return $this;
    }

    public function getDateFinDassurance(): ?\DateTimeInterface
    {
        return $this->dateFinDassurance;
    }

    public function setDateFinDassurance(?\DateTimeInterface $dateFinDassurance): self
    {
        $this->dateFinDassurance = $dateFinDassurance;

        return $this;
    }

    public function getMarque(): ?Marque
    {
        return $this->marque;
    }

    public function setMarque(?Marque $marque): self
    {
        $this->marque = $marque;

        return $this;
    }

    /**
     * @return Collection|Users[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(Users $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
        }

        return $this;
    }

    public function removeUser(Users $user): self
    {
        if ($this->user->contains($user)) {
            $this->user->removeElement($user);
        }

        return $this;
    }
}
