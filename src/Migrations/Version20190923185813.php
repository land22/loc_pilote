<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190923185813 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(15) DEFAULT NULL, prenom VARCHAR(15) DEFAULT NULL, email VARCHAR(20) NOT NULL, telephone INT DEFAULT NULL, date_arrive DATETIME DEFAULT NULL, date_depart DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vehicules (id INT AUTO_INCREMENT NOT NULL, matriculation VARCHAR(10) NOT NULL, nom VARCHAR(30) NOT NULL, url_photo VARCHAR(255) NOT NULL, couleur VARCHAR(10) DEFAULT NULL, prix_location INT NOT NULL, note VARCHAR(255) DEFAULT NULL, active TINYINT(1) DEFAULT NULL, modele VARCHAR(30) DEFAULT NULL, date_vidange DATETIME DEFAULT NULL, date_visite_technique DATETIME DEFAULT NULL, date_fin_dassurance DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE marque (id INT AUTO_INCREMENT NOT NULL, nom_marque VARCHAR(20) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE vehicules');
        $this->addSql('DROP TABLE marque');
    }
}
